package com.example.g3_sr_redditclone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class G3SrRedditCloneApplication {

    public static void main(String[] args) {
        SpringApplication.run(G3SrRedditCloneApplication.class, args);
    }

}
