package com.example.g3_sr_redditclone.repository;

import com.example.g3_sr_redditclone.model.Comment;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository {

    @Select("select * from comment")
    List<Comment> getAllComment();

    @Select("select * from comment where comment_id=#{id}")
    Comment findCommentById(int id);

    @Select("select * from comment where post_id=#{id}")
    Comment findCommentByPostId(int id);


    @Insert("insert into comment (comment_text) " +
            "values (#{comment.comment_text})")
    public Boolean insertComment(@Param("comment") Comment comment);


}
