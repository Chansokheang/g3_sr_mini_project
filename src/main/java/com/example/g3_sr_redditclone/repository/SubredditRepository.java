package com.example.g3_sr_redditclone.repository;

import com.example.g3_sr_redditclone.model.Subreddit;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface SubredditRepository {


    @Select("select * from subreddit")
    List<Subreddit> getAllSubreddit();


    @Select("select * from subreddit where subreddit_id=#{id}")
    public Subreddit findSubredditById(int id);


    //not yet working
    @Update("update subreddit set subreddit_title=#{subreddit.subreddit_title}" +
            "where subreddit_id = ${subreddit.subreddit_id}")
    public Boolean updateSubreddit(@Param("subreddit") Subreddit subreddit);


    @Delete("delete from subreddit where subreddit_id=#{id}")
    public Boolean deleteSubreddit(int id);

    @Insert("insert into subreddit (subreddit_title) values(#{subreddit.subreddit_title})")
    public Boolean insertSubreddit(@Param("subreddit") Subreddit subreddit);

}
