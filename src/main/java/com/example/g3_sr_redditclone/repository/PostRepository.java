package com.example.g3_sr_redditclone.repository;

import com.example.g3_sr_redditclone.model.Post;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository {

    @Select("select * from post")
    @Results({
            @Result(property = "post_id", column = "post_id"),
            @Result(property = "post_title", column = "post_title"),
            @Result(property = "post_description", column = "post_description"),
            @Result(property = "post_url", column = "post_url"),
            @Result(property = "post_last_update", column = "last_update"),
            @Result(property = "vote_count", column = "vote_count"),
            @Result(property = "subreddit", column = "subreddit_id", many = @Many(select = "com.example.g3_sr_redditclone.repository.SubredditRepository.findSubredditById")),
            @Result(property = "comment", column = "post_id", many = @Many(select = "com.example.g3_sr_redditclone.repository.CommentRepository.findCommentByPostId"))
    })
    List<Post> getAllPost();



    @Select("select * from post where post_id = #{post_id}")
    @Results({
            @Result(property = "post_id", column = "post_id"),
            @Result(property = "post_title", column = "post_title"),
            @Result(property = "post_description", column = "post_description"),
            @Result(property = "post_url", column = "post_url"),
            @Result(property = "post_last_update", column = "last_update"),
            @Result(property = "vote_count", column = "vote_count"),
            @Result(property = "comment", column = "post_id", many = @Many(select = "com.example.g3_sr_redditclone.repository.CommentRepository.findCommentByPostId"))
    })
    List<Post> getPostAndComment(@Param("id") int id);

    @Insert("insert into post (post_title,post_description, post_url,subreddit_id)" +
            "values(#{post.post_title},#{post.post_description}, #{post.post_url},#{post.subreddit_id})")
    public Boolean insertPost(@Param("post") Post post);

    @Update("update post set post_title=#{post.post_title}," +
            "post_description=#{post.post_description}, post_url=#{post.post_url},subreddit_id=#{subreddit_id}" +
            "where post_id=#{post_id}")
    public Boolean updatePost(@Param("post")Post post);

}
