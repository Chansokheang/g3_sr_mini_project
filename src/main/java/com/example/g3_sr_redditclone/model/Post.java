package com.example.g3_sr_redditclone.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@NoArgsConstructor
@Data
public class Post {
    private int post_id;
    @NotEmpty(message = "Please provide a title")
    private String post_title;
    @NotEmpty(message = "Please provide a description")
    private String post_description;
    private String post_url;
    private String post_last_update;
    private int subreddit_id;
    private String post_create_date;
    private int vote_count;
    private MultipartFile file;
    private List<Comment> comment;
    private List<Subreddit> subreddit;

    public Post(String post_title, String post_description, String post_url, int subreddit_id) {
        this.post_title = post_title;
        this.post_description = post_description;
        this.post_url = post_url;
        this.subreddit_id = subreddit_id;
    }
}
