package com.example.g3_sr_redditclone.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import java.sql.Date;

@NoArgsConstructor
@Data
public class Comment {
    private int comment_id;
    @NotEmpty(message = "Please provide a text")
    private String comment_text;
    private String comment_last_update;
    private int post_id;
    private int vote_count;

    public Comment(String comment_text) {
        this.comment_text = comment_text;
    }
}
