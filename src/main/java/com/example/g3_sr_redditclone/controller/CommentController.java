package com.example.g3_sr_redditclone.controller;


import com.example.g3_sr_redditclone.model.Comment;
import com.example.g3_sr_redditclone.model.Post;
import com.example.g3_sr_redditclone.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CommentController {

    @Autowired
    CommentService commentService;

//    @GetMapping("/comment")
//    public String getAllComment(){
//        System.out.println("hi");
//
//        Comment comment = new Comment("this is 10");
//        Boolean test = commentService.insertComment(comment);
//        System.out.println(test);
//
//        commentService.getAllComment().stream().forEach(System.out::println);

//        System.out.println(commentService.findCommentById(1));

//        return "index";
//    }

    @GetMapping("/comment")
    public String addComment(Model model){
        model.addAttribute("comment", new Comment());
        return "index";
    }

    @PostMapping("/handle-addComment")
    public String handleAddComment(@ModelAttribute Comment comment){
        System.out.println("testing comment");
        commentService.insertComment(comment);
        return "redirect:/comment";
    }

}
