package com.example.g3_sr_redditclone.service.serviceImp;


import com.example.g3_sr_redditclone.model.Post;
import com.example.g3_sr_redditclone.repository.PostRepository;
import com.example.g3_sr_redditclone.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImp implements PostService {

    @Autowired
    PostRepository postRepository;

    @Override
    public List<Post> getAllPost() {
        return postRepository.getAllPost();
    }

    @Override
    public List<Post> getPostAndComment(int id) {
        return postRepository.getPostAndComment(id);
    }


    @Override
    public Boolean inserPost(Post post) {
        return postRepository.insertPost(post);
    }

    @Override
    public Boolean updatePost(Post post) {
        return postRepository.updatePost(post);
    }

}
