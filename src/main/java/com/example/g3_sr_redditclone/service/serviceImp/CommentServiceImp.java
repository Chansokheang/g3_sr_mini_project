package com.example.g3_sr_redditclone.service.serviceImp;

import com.example.g3_sr_redditclone.model.Comment;
import com.example.g3_sr_redditclone.repository.CommentRepository;
import com.example.g3_sr_redditclone.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImp implements CommentService {

    @Autowired
    CommentRepository commentRepository;


    @Override
    public List<Comment> getAllComment() {
        return commentRepository.getAllComment();
    }

    @Override
    public Comment findCommentById(int id) {
        return commentRepository.findCommentById(id);
    }

    @Override
    public Comment findCommentByPostId(int id) {
        return commentRepository.findCommentByPostId(id);
    }

    @Override
    public Boolean insertComment(Comment comment) {
        return commentRepository.insertComment(comment);
    }
}
