package com.example.g3_sr_redditclone.service.serviceImp;

import com.example.g3_sr_redditclone.model.Subreddit;
import com.example.g3_sr_redditclone.repository.SubredditRepository;
import com.example.g3_sr_redditclone.service.SubredditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SubredditServiceImp implements SubredditService {

    @Autowired
    SubredditRepository subredditRepository;

    @Override
    public List<Subreddit> getAllSubreddit() {
        return subredditRepository.getAllSubreddit();
    }

    @Override
    public Subreddit findSubredditById(int id) {
        return subredditRepository.findSubredditById(id);
    }

    @Override
    public Boolean updateSubreddit(Subreddit subreddit) {
        return subredditRepository.updateSubreddit(subreddit);
    }

    @Override
    public Boolean deleteSubreddit(int id) {
        return subredditRepository.deleteSubreddit(id);
    }

    @Override
    public Boolean insertSubreddit(Subreddit subreddit) {
        return subredditRepository.insertSubreddit(subreddit);
    }
}
