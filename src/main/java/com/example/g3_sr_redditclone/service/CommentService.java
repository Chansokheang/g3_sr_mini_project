package com.example.g3_sr_redditclone.service;


import com.example.g3_sr_redditclone.model.Comment;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentService {

    List<Comment> getAllComment();

    Comment findCommentById(int id);

    Comment findCommentByPostId(int id);

    Boolean insertComment(Comment comment);
}
