package com.example.g3_sr_redditclone.service;


import com.example.g3_sr_redditclone.model.Post;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PostService {


    List<Post> getAllPost();

    List<Post> getPostAndComment(int id);

    Boolean inserPost(Post post);

    Boolean updatePost(Post post);

}
